<%-- 
    Document   : RegistrarRestaurante
    Created on : 13/04/2017, 09:36:15 AM
    Author     : Jose
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="util.Conexion"%>
<%@page import="vo.Admin"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Administrador | Registrar Restaurante</title>

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
        <link rel="stylesheet" href="css/sweetalert.css">
        <link rel="stylesheet" href="css/style.css">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <script src="js/jquery-3.2.1.min.js"></script>
    </head>
    <body>
        <!-- Nav Lateral -->
        <section class="NavLateral full-width">
            <div class="NavLateral-FontMenu full-width ShowHideMenu"></div>
            <div class="NavLateral-content full-width">
                <header class="NavLateral-title full-width center-align">
                    Administrador<i class="zmdi zmdi-close NavLateral-title-btn ShowHideMenu"></i>
                </header>
                <figure class="full-width NavLateral-logo">
                    <img src="assets/img/logo.jpg" alt="material-logo" class="responsive-img center-box">
                    <figcaption class="center-align">Restaurante USApp</figcaption>
                </figure> 
                <div class="NavLateral-Nav">
                    <ul class="full-width">
                        <li>
                            <a href="/RestaurantesWeb/HomeAdmin.jsp" class="waves-effect waves-light"><i class="zmdi zmdi-home zmdi-hc-fw"></i> Home</a>
                        </li>
                        <li>
                            <a href="/RestaurantesWeb/RegistrarRestaurante.jsp" class="waves-effect waves-light"><i class="zmdi zmdi-assignment"></i> Registrar Restaurante</a>
                        </li>
                        <li>
                            <a href="/RestaurantesWeb/RegistrarPlato.jsp" class="waves-effect waves-light"><i class="zmdi zmdi-cutlery"></i> Registrar Plato</a>
                        </li>
                        <li>
                            <a href="/RestaurantesWeb/RestaurantesControlador" class="waves-effect waves-light"><i class="zmdi zmdi-view-list-alt"></i> Ver Restaurantes </a>
                        </li>
                        <li>
                            <a href="/RestaurantesWeb/PlatoControlador" class="waves-effect waves-light"><i class="zmdi zmdi-view-list-alt"></i> Ver Platos</a>
                        </li>
                        <li>
                            <a href="/RestaurantesWeb/UsuarioControlador" class="waves-effect waves-light"><i class="zmdi zmdi-accounts-list"></i> Ver Usuarios</a>
                        </li>

                    </ul>
                </div>  
            </div>  
        </section>

        <%
            HttpSession sessionUser = request.getSession(false);
            String us = (String) sessionUser.getAttribute("nick");

            Admin admin = new Admin();
            admin.setNick(us);
            admin.GetAdmin();

            //            out.print("Bienvenido ");
            //            out.print(usuario.getNombre());
            //            out.print("!!!");
        %>
        <!-- Page content -->
        <section class="ContentPage full-width">
            <!-- Nav Info -->
            <div class="ContentPage-Nav full-width">
                <ul class="full-width">
                    <li class="btn-MobileMenu ShowHideMenu"><a href="#" class="tooltipped waves-effect waves-light" data-position="bottom" data-delay="50" data-tooltip="Menu"><i class="zmdi zmdi-more-vert"></i></a></li>
                    <li><figure><img src="assets/img/user.png" alt="UserImage"></figure></li>
                    <li style="padding:0 5px;"><%out.print(admin.getNombre());%></li>
                    <li><a href="#" class="tooltipped waves-effect waves-light btn-ExitSystem" data-position="bottom" data-delay="50" data-tooltip="Logout"><i class="zmdi zmdi-power"></i></a></li>
                    <li><a href="#" class="tooltipped waves-effect waves-light btn-Search" data-position="bottom" data-delay="50" data-tooltip="Search"><i class="zmdi zmdi-search"></i></a></li>

                </ul>   
            </div>

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">
                            Ingresar datos para registrar nuevo Restaurante:
                        </h3>

                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-6">
                        <form action="RestaurantesControlador" method="POST">

                            <div class="form-group">
                                <label>Nombre:</label>
                                <input class="form-control" placeholder="Enter text" type="text" name="nombre">
                            </div>

                            <div class="form-group">
                                <label>Direccion:</label>
                                <input class="form-control" placeholder="Enter text" type="text" name="direccion">
                            </div>
                            
                            <div class="form-group"><!--FALTA OBTENER EL Valor PARA GUARDARLO EN EN la BD-->
                                <label>Tipo:</label>
                                <select class="form-control" name="tipo">
                                    <%
                                        Conexion dbconn = new Conexion();

                                        try {
                                            Connection myconnection = dbconn.getConnection();
                                            String sqlString = "select distinct restaurante.tipo from restaurante;";
                                            Statement myStatement = myconnection.createStatement();
                                            ResultSet rs = myStatement.executeQuery(sqlString);
                                            while (rs.next()) {
                                                out.println("<option>" + rs.getString(1) + "</option>");
                                            }

                                        } catch (Exception e) {
                                            out.println("Entrada incorrecta" + e);
                                        }

                                    %>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Calificacion</label>
                                <label class="radio-inline">
                                    <input type="radio" name="calificacion" id="optionsRadiosInline1" value="1" checked>1
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="calificacion" id="optionsRadiosInline2" value="2" >2
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="calificacion" id="optionsRadiosInline3" value="3" >3
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="calificacion" id="optionsRadiosInline3" value="4" >4
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="calificacion" id="optionsRadiosInline3" value="5" >5
                                </label>
                                <br>
                                <br>
                            </div>
                            <button id="regis" type="submit" class="btn btn-default">Registrar</button>
                            <button type="reset" class="btn btn-default">Limpiar</button>
                        </form>
                        <br>            
                    </div>
                </div>
                <!-- /.row -->
            </div>

            <div class="NavLateralDivider"></div>
            <div class="footer-copyright">
                <div class="container center-align">
                    © 2017 Restaurantes USA Web App
                </div>
            </div>
        </section>



        <!-- Sweet Alert JS -->
        <script src="js/sweetalert.min.js"></script>

        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/jquery-2.2.0.min.js"><\/script>')</script>

        <!-- Materialize JS -->
        <script src="js/materialize.min.js"></script>

        <!-- Malihu jQuery custom content scroller JS -->
        <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>

        <!-- MaterialDark JS -->
        <script src="js/main.js"></script>
        <script src="js/registroRes.js"></script>
    </body>
</html>
