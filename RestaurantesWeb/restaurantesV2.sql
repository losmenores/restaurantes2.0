drop database if exists restaurantesV2;

create database restaurantesV2;

use restaurantesV2;

CREATE TABLE usuario (
  
  id int(10) auto_increment,
  nombre varchar(50) NOT NULL,
  correo varchar(25) NOT NULL,
  nick varchar(15) NOT NULL,
  contrasena varchar(25) NOT NULL,
  primary key(id)
  ); 

CREATE TABLE restaurante (
  id int(5) auto_increment,
  nombre varchar(50) NOT NULL,
  direccion varchar(50) NOT NULL,
  tipo varchar(25) NOT NULL,
  calificacion int(5),
  primary key(id)
);

drop table if exists plato;
CREATE TABLE plato (
  id_plato int(5) auto_increment,
  id_restaurante int(5),
  nombrePlato varchar(50) not null,
  precio int(11) NOT NULL,
  primary key(id_plato), 
  foreign key(id_restaurante) references restaurante(id)
);
truncate table plato;

CREATE TABLE admin(
	
  nombre varchar(50) NOT NULL,
  correo varchar(25) NOT NULL,
  nick varchar(15) NOT NULL,
  contrasena varchar(25) NOT NULL,
  primary key(nick)
  ); 
  

INSERT INTO admin(nombre,correo,nick,contrasena) VALUES ('Jose Paredes','admin@gmail','admin','999');

#INSERTANDO RESTAURANTES
INSERT INTO restaurante(id,nombre,direccion,tipo,calificacion) VALUES
(1, 'clavos y canela', '74 #14-59', 'corriente', 3),
(2, 'mr toro', '75 #14-50', 'corriente', 3);


#INSERTANDO PLATOS
INSERT INTO plato(id_plato,id_restaurante,nombrePlato,precio) VALUES ('1','1','Pizza All the meats',9200);
INSERT INTO plato(id_plato,id_restaurante,nombrePlato,precio) VALUES ('2','2','Hamburguesa de pollo',7500);
INSERT INTO plato(id_plato,id_restaurante,nombrePlato,precio) VALUES ('3','1','Cinnamon Roll ',3000);

INSERT INTO usuario(nombre,correo,nick,contrasena) VALUES ('Carlos Jimenez','carlosj@gmail.com','carlos10','123');


