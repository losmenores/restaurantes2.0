/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import dao.DaoRestaurante;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import vo.Restaurante;

/**
 *
 * @author Jose
 */
public class RestaurantesControlador extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RestaurantesControlador</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RestaurantesControlador at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ArrayList<Restaurante> lista = null;
        DaoRestaurante daoRest = new DaoRestaurante();
        //lista de elementos
        lista = daoRest.listarTodo();
        //Envio de datos 
        request.setAttribute("restaurantes", lista);
        //RequestDispatcher
        RequestDispatcher rd
                = request.getRequestDispatcher("ListaRestaurantes.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String nombre = request.getParameter("nombre");
        String direccion = request.getParameter("direccion");
        String tipo = request.getParameter("tipo");
        String calificacion = request.getParameter("calificacion");
        
        Restaurante restaurante = new Restaurante();
        
        restaurante.setNombre(nombre);
        restaurante.setDireccion(direccion);
        restaurante.setTipo(tipo);
        restaurante.setCalificacion(Integer.parseInt(calificacion));
        
        DaoRestaurante daoRestaurante = new DaoRestaurante();
        
        boolean resultado = daoRestaurante.insertar(restaurante);
        if(resultado == false){
            request.setAttribute("insercion", "OK");
            
        }else{
            request.setAttribute("insercion", "NO");
            
        }
        RequestDispatcher rd = request.getRequestDispatcher("RegistrarRestaurante.jsp");
        rd.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
