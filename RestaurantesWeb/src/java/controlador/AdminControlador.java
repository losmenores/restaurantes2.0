/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import dao.DaoUsuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import vo.Admin;
import vo.Usuario;

/**
 *
 * @author Jose
 */
public class AdminControlador extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        PrintWriter out = response.getWriter();
        
        try {
            Admin admin = new Admin();

            admin.setNick(request.getParameter("nick"));
            admin.setPassword(request.getParameter("password"));
            
            if (Admin.LoginAdmin(request.getParameter("nick"), request.getParameter("password"))) {
                Admin ad = new Admin();
                ad.setNick(String.valueOf(request.getParameter("nick")));
                ad.GetAdmin();

                HttpSession sessionUser = request.getSession();
                sessionUser.setAttribute("nick", ad.getNick());

                RequestDispatcher rd1 = request.getRequestDispatcher("HomeAdmin.jsp");
                rd1.forward(request, response);
            } else {
                out.println("Nombre de usuario o contraseña incorrecto!");
                out.println("<a href=\"IniciarSesion.jsp\">Intentar de nuevo...</a>");

            }
        } finally {out.close();}

       
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ArrayList<Usuario> lista = null;
        DaoUsuario daoUser = new DaoUsuario();
        //lista de elementos
        lista = daoUser.listarTodo();
        //Envio de datos 
        request.setAttribute("usuarios", lista);
        //RequestDispatcher
        RequestDispatcher rd
                = request.getRequestDispatcher("ListaUsuarios.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
