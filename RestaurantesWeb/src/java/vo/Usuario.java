package vo;

/**
 *
 * @author Jose
 */
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.Conexion;

public class Usuario {

    private String nombre, correo, nick, password;
    private int id;
    
    public Usuario() {
        nombre = "";
        correo = "";
        nick = "";
        password = "";
        id = 0;
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public void RegistrarUsuario() {
        try {
            Conexion dbconn = new Conexion();
            Connection myconnection = dbconn.getConnection();

            String sqlString = "INSERT INTO usuario (nombre,correo,nick,contrasena) VALUES ('" + nombre + "','" + correo + "','" + nick + "','" + password + "')";

            Statement myStatement = myconnection.createStatement();

            try {
                myStatement.executeUpdate(sqlString);
                myStatement.close();
                myconnection.close();
            } catch (SQLException ex) {
                Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static boolean LoginUsuario(String nick, String password) {
        boolean check = false;
        try {
            Conexion dbconn = new Conexion();
            Connection myconnection = dbconn.getConnection();

            PreparedStatement ps1 = myconnection.prepareStatement("select * from usuario where nick=? and contrasena=?");

            ps1.setString(1, nick);
            ps1.setString(2, password);
            ResultSet rs1 = ps1.executeQuery();
            check = rs1.next();

            myconnection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return check;
    }

    public void GetUsuario() {
        try {
            Conexion dbconn = new Conexion();
            Connection myconnection = dbconn.getConnection();

            String sqlString = "SELECT * FROM usuario WHERE nick = '" + nick + "'";
            Statement myStatement = myconnection.createStatement();
            ResultSet rs = myStatement.executeQuery(sqlString);

            while (rs.next()) {
                
                nombre = rs.getString("nombre");
                correo = rs.getString("correo");
                nick = rs.getString("nick");
                password = rs.getString("contrasena");
            }

            myStatement.close();
            myconnection.close();

        } catch (SQLException ex) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}

