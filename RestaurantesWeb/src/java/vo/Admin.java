package vo;

/**
 *
 * @author Jose
 */

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.Conexion;

public class Admin {

        private String nombre, correo, nick, password;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
        
        public static boolean LoginAdmin(String nick, String password) {
        boolean check = false;
        try {
            Conexion dbconn = new Conexion();
            Connection myconnection = dbconn.getConnection();

            PreparedStatement ps1 = myconnection.prepareStatement("select * from admin where nick=? and contrasena=?");

            ps1.setString(1, nick);
            ps1.setString(2, password);
            ResultSet rs1 = ps1.executeQuery();
            check = rs1.next();

            myconnection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return check;
    }

    public void GetAdmin() {
        try {
            Conexion dbconn = new Conexion();
            Connection myconnection = dbconn.getConnection();

            String sqlString = "SELECT * FROM admin WHERE nick = '" + nick + "'";
            Statement myStatement = myconnection.createStatement();
            ResultSet rs = myStatement.executeQuery(sqlString);

            while (rs.next()) {
                
                nombre = rs.getString("nombre");
                correo = rs.getString("correo");
                nick = rs.getString("nick");
                password = rs.getString("contrasena");
            }

            myStatement.close();
            myconnection.close();

        } catch (SQLException ex) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    
}
