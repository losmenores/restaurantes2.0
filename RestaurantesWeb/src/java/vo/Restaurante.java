package vo;

/**
 *
 * @author Jose
 */
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.Conexion;

public class Restaurante {

    private String nombre, direccion, tipo;
    private int calificacion, id;

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    
    
    public void GetRestaurante() {
        try {
            Conexion dbconn = new Conexion();
            Connection myconnection = dbconn.getConnection();

            String sqlString = "SELECT * FROM restaurante WHERE id = '" + id + "'";
            Statement myStatement = myconnection.createStatement();
            ResultSet rs = myStatement.executeQuery(sqlString);

            while (rs.next()) {
                
                id = rs.getInt("id");
                nombre = rs.getString("nombre");
                direccion = rs.getString("direccion");
                tipo = rs.getString("tipo");
                calificacion = rs.getInt("calificacion");
            }

            myStatement.close();
            myconnection.close();

        } catch (SQLException ex) {
            Logger.getLogger(Restaurante.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
