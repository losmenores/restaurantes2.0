/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import vo.Restaurante;
import util.Conexion;

/**
 *
 * @author Jose
 */
public class DaoRestaurante {

    private Connection conexion;

    public DaoRestaurante() {

        Conexion db = Conexion.getConexion();
        this.conexion = db.getConnection();
    }

    public boolean insertar(Restaurante restaurante) {
        boolean resultado = false;
        try {
            //1.Establecer la consulta
            String consulta = "INSERT INTO restaurante (id,nombre,direccion,tipo,calificacion) VALUES (null,?,?,?,?);";
            //2. Crear el PreparedStament
            PreparedStatement statement
                    = this.conexion.prepareStatement(consulta);
            //-----------------------------------
            statement.setString(1, restaurante.getNombre());
            statement.setString(2, restaurante.getDireccion());
            statement.setString(3, restaurante.getTipo());
            statement.setInt(4, restaurante.getCalificacion());
            //--------------------------------------
            //3. Hacer la ejecucion
            resultado = statement.execute();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return resultado;
    }

    public ArrayList<Restaurante> listarTodo() {
        //1.Consulta
        ArrayList<Restaurante> respuesta = new ArrayList();
        String consulta = "SELECT * FROM restaurante";
        try {
            //----------------------------
            //Statement
            Statement statement
                    = this.conexion.createStatement();
            //Ejecucion
            ResultSet resultado
                    = statement.executeQuery(consulta);
            //----------------------------
            //Recorrido sobre el resultado
            while (resultado.next()) {

                Restaurante restaurante = new Restaurante();
                restaurante.setId(resultado.getInt("id"));
                restaurante.setNombre(resultado.getString("nombre"));
                restaurante.setDireccion(resultado.getString("direccion"));
                restaurante.setTipo(resultado.getString("tipo"));
                restaurante.setCalificacion(resultado.getInt("calificacion"));
                
                respuesta.add(restaurante);

            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return respuesta;
    }

    

}
