/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import vo.Plato;
import vo.Restaurante;
import util.Conexion;

/**
 *
 * @author Jose
 */
public class DaoPlato {
    
    private Connection conexion;
    
    public DaoPlato(){
        
        Conexion db = Conexion.getConexion();
        this.conexion = db.getConnection();
    }
    
    public boolean insertar(Plato plato) {
        boolean resultado = false;
        try {
            //1.Establecer la consulta
            String consulta = "INSERT INTO plato (id_plato,id_restaurante,nombrePlato,precio) VALUES(null,?,?,?)";
            //2. Crear el PreparedStament
            PreparedStatement statement
                    = this.conexion.prepareStatement(consulta);
            //-----------------------------------
            
            statement.setInt(1, plato.getIdRest());
            statement.setString(2, plato.getNombre());
            statement.setDouble(3, plato.getCosto());
            
            
            //--------------------------------------
            //3. Hacer la ejecucion
            resultado = statement.execute();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return resultado;
    }

    public ArrayList<Plato> listarTodo() {
        //1.Consulta
        ArrayList<Plato> respuesta = new ArrayList();
        String consulta = "select * from restaurante, plato where restaurante.ID = plato.id_restaurante;";
        try {
            //----------------------------
            //Statement
            Statement statement
                    = this.conexion.createStatement();
            //Ejecucion
            ResultSet resultado
                    = statement.executeQuery(consulta);
            //----------------------------
            //Recorrido sobre el resultado
            while (resultado.next()) {

                Plato plato = new Plato();
                
                plato.setNombreRestaurante(resultado.getString("nombre"));
                plato.setNombre(resultado.getString("nombrePlato"));
                plato.setCosto(resultado.getDouble("precio"));
                respuesta.add(plato);

            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return respuesta;
    }

}
