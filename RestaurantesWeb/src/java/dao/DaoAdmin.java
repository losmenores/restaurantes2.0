package dao;

/**
 *
 * @author Jose
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import vo.Admin;
import util.Conexion;

public class DaoAdmin {
        
    private Connection conexion;
    
    public DaoAdmin(){
        Conexion db = Conexion.getConexion();
        this.conexion = db.getConnection();
    }
    
    
        public static boolean LoginAdmin(String nick, String password) {
            boolean check = false;
            try {
                Conexion dbconn = new Conexion();
                Connection myconnection = dbconn.getConnection();

                PreparedStatement ps1 = myconnection.prepareStatement("select * from admin where nick=? and contrasena=?");

                ps1.setString(1, nick);
                ps1.setString(2, password);
                ResultSet rs1 = ps1.executeQuery();
                check = rs1.next();

                myconnection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return check;
    }

    
}