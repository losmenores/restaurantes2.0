/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import vo.Usuario;
import util.Conexion;

/**
 *
 * @author Jose
 */


public class DaoUsuario {

    private Connection conexion;
    
    public DaoUsuario(){
            
        Conexion db = Conexion.getConexion();
        this.conexion = db.getConnection();
    }
    
    public boolean insertar(Usuario usuario) {
        boolean resultado = false;
        try {
            //1.Establecer la consulta
            String consulta = "INSERT INTO usuario(id,nombre,correo,nick,contrasena) VALUES(null,?,?,?,?)";
            //2. Crear el PreparedStament
            PreparedStatement statement
                    = this.conexion.prepareStatement(consulta);
            //-----------------------------------
            statement.setString(1, usuario.getNombre());
            statement.setString(2, usuario.getCorreo());
            statement.setString(3, usuario.getNick());
            statement.setString(4, usuario.getPassword());
            //--------------------------------------
            //3. Hacer la ejecucion
            resultado = statement.execute();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return resultado;
    }
    
     public ArrayList<Usuario> listarTodo() {
        //1.Consulta
        ArrayList<Usuario> respuesta = new ArrayList();
        String consulta = "SELECT * FROM usuario";
        try { 
            //----------------------------
            //Statement
            Statement statement
                    = this.conexion.createStatement();
            //Ejecucion
            ResultSet resultado
                    = statement.executeQuery(consulta);
            //----------------------------
            //Recorrido sobre el resultado
            while (resultado.next()) {

                Usuario usuario = new Usuario();
                usuario.setId(Integer.parseInt(resultado.getString("id")));
                usuario.setNombre(resultado.getString("nombre"));
                usuario.setCorreo(resultado.getString("correo"));
                usuario.setNick(resultado.getString("nick"));
                usuario.setPassword(resultado.getString("contrasena"));
                    
                respuesta.add(usuario);

            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return respuesta;
    }
     
     
}
