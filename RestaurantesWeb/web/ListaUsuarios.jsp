<%-- 
    Document   : ListaUsuarios
    Created on : 13/04/2017, 07:26:44 AM
    Author     : Jose
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="vo.Admin"%>
<%@page import="vo.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>Administrador | Home</title>
    
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/materialize.min.css">
    	<link rel="stylesheet" href="css/material-design-iconic-font.min.css">
    	<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
        <link rel="stylesheet" href="css/sweetalert.css">
        <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <!-- Nav Lateral -->
    <section class="NavLateral full-width">
        <div class="NavLateral-FontMenu full-width ShowHideMenu"></div>
        <div class="NavLateral-content full-width">
            <header class="NavLateral-title full-width center-align">
                Administrador<i class="zmdi zmdi-close NavLateral-title-btn ShowHideMenu"></i>
            </header>
            <figure class="full-width NavLateral-logo">
                <img src="assets/img/logo.jpg" alt="material-logo" class="responsive-img center-box">
                <figcaption class="center-align">Restaurante USApp</figcaption>
            </figure> 
            <div class="NavLateral-Nav">
                <ul class="full-width">
                    <li>
                        <a href="/RestaurantesWeb/HomeAdmin.jsp" class="waves-effect waves-light"><i class="zmdi zmdi-home zmdi-hc-fw"></i> Home</a>
                    </li>
                    <li>
                        <a href="/RestaurantesWeb/HomeAdmin.jsp" class="waves-effect waves-light"><i class="zmdi zmdi-assignment"></i> Registrar Restaurante</a>
                    </li>
                    <li>
                        <a href="/RestaurantesWeb/HomeAdmin.jsp" class="waves-effect waves-light"><i class="zmdi zmdi-cutlery"></i> Registrar Plato</a>
                    </li>
                    <li>
                        <a href="/RestaurantesWeb/HomeAdmin.jsp" class="waves-effect waves-light"><i class="zmdi zmdi-view-list-alt"></i> Ver Restaurantes </a>
                    </li>
                    <li>
                        <a href="/RestaurantesWeb/HomeAdmin.jsp" class="waves-effect waves-light"><i class="zmdi zmdi-accounts-list"></i> Ver Usuarios</a>
                    </li>
                    
                </ul>
            </div>  
        </div>  
    </section>
    
    <%
            HttpSession sessionUser = request.getSession(false);
            String us = (String) sessionUser.getAttribute("nick");

            Admin admin = new Admin();
            admin.setNick(us);
            admin.GetAdmin();

//            out.print("Bienvenido ");
//            out.print(usuario.getNombre());
//            out.print("!!!");
    %>
    <!-- Page content -->
    <section class="ContentPage full-width">
        <!-- Nav Info -->
        <div class="ContentPage-Nav full-width">
            <ul class="full-width">
                <li class="btn-MobileMenu ShowHideMenu"><a href="#" class="tooltipped waves-effect waves-light" data-position="bottom" data-delay="50" data-tooltip="Menu"><i class="zmdi zmdi-more-vert"></i></a></li>
                <li><figure><img src="assets/img/user.png" alt="UserImage"></figure></li>
                <li style="padding:0 5px;"><%out.print(admin.getNombre());%></li>
                <li><a href="#" class="tooltipped waves-effect waves-light btn-ExitSystem" data-position="bottom" data-delay="50" data-tooltip="Logout"><i class="zmdi zmdi-power"></i></a></li>
                <li><a href="#" class="tooltipped waves-effect waves-light btn-Search" data-position="bottom" data-delay="50" data-tooltip="Search"><i class="zmdi zmdi-search"></i></a></li>
                
            </ul>   
        </div>
                <h3>Lista de usuarios</h3>
        
                <div>
                    <table>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Correo</th>
                            <th>Nick</th>
                            <th>Password</th>

                        </tr>
                        <%
                            if (request.getAttribute("usuarios") != null) {
                                ArrayList<Usuario> usuarios = (ArrayList<Usuario>) request.getAttribute("usuarios");
                                for (Usuario user : usuarios) {
                        %>
                        <tr>
                            <td><%=user.getId()%></td>           
                            <td><%=user.getNombre()%></td>           
                            <td><%=user.getCorreo()%></td>           
                            <td><%=user.getNick()%></td>   
                            <td><%=user.getPassword()%></td>   

                        </tr>
                        <%
                                }
                            }
                        %>
                    </table> 
                </div>
                    <br>    
                    <br>    
                    <br>    
                    <br>    
                    
                
            <div class="NavLateralDivider"></div>
            <div class="footer-copyright">
                <div class="container center-align">
                    © 2017 Restaurantes USA Web App
                </div>
            </div>
    </section>
        
    
    
    <!-- Sweet Alert JS -->
    <script src="js/sweetalert.min.js"></script>
    
    <!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/jquery-2.2.0.min.js"><\/script>')</script>
    
    <!-- Materialize JS -->
	<script src="js/materialize.min.js"></script>
    
    <!-- Malihu jQuery custom content scroller JS -->
	<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    
    <!-- MaterialDark JS -->
	<script src="js/main.js"></script>
</body>
</html>
