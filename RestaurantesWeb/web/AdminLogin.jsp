<%-- 
    Document   : Admin
    Created on : 26/03/2017, 18:43:10
    Author     : Jose
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="vo.Admin"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Restaurantes App | Administrador</title>
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/materialize.min.css">
        <link rel="stylesheet" href="css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
        <link rel="stylesheet" href="css/sweetalert.css">
        <link rel="stylesheet" href="css/style.css">
        <script src="js/jquery-3.2.1.min.js"></script>

    </head>
    <body class="font-cover" id="admin">
        
        <div class="container-login center-align">
            <div style="margin:15px 0;">
                <i class="zmdi zmdi-account-circle zmdi-hc-5x"></i>
                <p>Inicia sesión como administrador</p>   
            </div>
            <form action="AdminControlador" method="post">
                <div class="input-field">
                    <input type="text" name="nick">
                    <label for="UserName"><i class="zmdi zmdi-account"></i>&nbsp; Usuario</label>
                </div>
                <div class="input-field col s12">
                    <input type="password" name="password">
                    <label for="Password"><i class="zmdi zmdi-lock"></i>&nbsp; Contraseña</label>
                </div>
                <button id="adm" class="waves-effect waves-teal btn-flat">Ingresar &nbsp; <i class="zmdi zmdi-mail-send"></i></button>
            </form>
            
        </div>
        <div>
            <a href="/RestaurantesWeb/index.html" class="btn btn-round btn-default">Home</a>
        </div>
        
        <!-- Sweet Alert JS -->
        <script src="js/sweetalert.min.js"></script>

        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/jquery-2.2.0.min.js"><\/script>')</script>

        <!-- Materialize JS -->
        <script src="js/materialize.min.js"></script>

        <!-- Malihu jQuery custom content scroller JS -->
        <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>

        <!-- MaterialDark JS -->
        <script src="js/main.js"></script>

        <script src="js/admlog.js"></script>
        
    </body>
</html>
