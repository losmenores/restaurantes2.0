<%-- 
    Document   : HomeAdmin
    Created on : 12/04/2017, 16:46:48
    Author     : Jose
--%>

<%@page import="vo.Admin"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Administrador | Home</title>

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/materialize.min.css">
        <link rel="stylesheet" href="css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
        <link rel="stylesheet" href="css/sweetalert.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <!-- Nav Lateral -->
        <section class="NavLateral full-width">
            <div class="NavLateral-FontMenu full-width ShowHideMenu"></div>
            <div class="NavLateral-content full-width">
                <header class="NavLateral-title full-width center-align">
                    Administrador<i class="zmdi zmdi-close NavLateral-title-btn ShowHideMenu"></i>
                </header>
                <figure class="full-width NavLateral-logo">
                    <img src="assets/img/logo.jpg" alt="material-logo" class="responsive-img center-box">
                    <figcaption class="center-align">Restaurante USApp</figcaption>
                </figure> 
                <div class="NavLateral-Nav">
                    <ul class="full-width">
                        <li>
                            <a href="/RestaurantesWeb/HomeAdmin.jsp" class="waves-effect waves-light"><i class="zmdi zmdi-home zmdi-hc-fw"></i> Home</a>
                        </li>
                        <li>
                            <a href="/RestaurantesWeb/RegistrarRestaurante.jsp" class="waves-effect waves-light"><i class="zmdi zmdi-assignment"></i> Registrar Restaurante</a>
                        </li>
                        <li>
                            <a href="/RestaurantesWeb/RegistrarPlato.jsp" class="waves-effect waves-light"><i class="zmdi zmdi-cutlery"></i> Registrar Plato</a>
                        </li>
                        <li>
                            <a href="/RestaurantesWeb/RestaurantesControlador" class="waves-effect waves-light"><i class="zmdi zmdi-view-list-alt"></i> Ver Restaurantes </a>
                        </li>
                        <li>
                            <a href="/RestaurantesWeb/PlatoControlador" class="waves-effect waves-light"><i class="zmdi zmdi-view-list-alt"></i> Ver Platos</a>
                        </li>
                        <li>
                            <a href="/RestaurantesWeb/UsuarioControlador" class="waves-effect waves-light"><i class="zmdi zmdi-accounts-list"></i> Ver Usuarios</a>
                        </li>

                    </ul>
                </div>  
            </div>  
        </section>

        <%
            HttpSession sessionUser = request.getSession(false);
            String us = (String) sessionUser.getAttribute("nick");

            Admin admin = new Admin();
            admin.setNick(us);
            admin.GetAdmin();

    
        %>
        <!-- Page content -->
        <section class="ContentPage full-width">
            <!-- Nav Info -->
            <div class="ContentPage-Nav full-width">
                <ul class="full-width">
                    <li class="btn-MobileMenu ShowHideMenu"><a href="#" class="tooltipped waves-effect waves-light" data-position="bottom" data-delay="50" data-tooltip="Menu"><i class="zmdi zmdi-more-vert"></i></a></li>
                    <li><figure><img src="assets/img/user.png" alt="UserImage"></figure></li>
                    <li style="padding:0 5px;"><%out.print(admin.getNombre());%></li>
                    <li><a href="#" class="tooltipped waves-effect waves-light btn-ExitSystem" data-position="bottom" data-delay="50" data-tooltip="Logout"><i class="zmdi zmdi-power"></i></a></li>
                    <li><a href="#" class="tooltipped waves-effect waves-light btn-Search" data-position="bottom" data-delay="50" data-tooltip="Search"><i class="zmdi zmdi-search"></i></a></li>

                </ul>   
            </div>

            <div class="row">
                <!-- Tiles -->
                <article class="col s12">
                    <div class="full-width center-align" style="margin: 80px 0;">
                        <div class="tile">
                            <div class="tile-icon"><i class="zmdi zmdi-edit"></i></div>
                            <div class="tile-caption">
                                <span class="center-align">Registrar</span>
                                <p class="center-align">Restaurantes</p>   
                            </div>
                            <a href="/RestaurantesWeb/RegistrarRestaurante.jsp" class="tile-link waves-effect waves-light"> Ir a registrar &nbsp; <i class="zmdi zmdi-caret-right-circle"></i></a>
                        </div>
                        <div class="tile">
                            <div class="tile-icon"><i class="zmdi zmdi-assignment"></i></div>
                            <div class="tile-caption">
                                <span class="center-align">Mostrar </span>
                                <p class="center-align">Restaurantes</p>   
                            </div>
                            <a href="/RestaurantesWeb/RestaurantesControlador" class="tile-link waves-effect waves-light">Ir a mostrar &nbsp; <i class="zmdi zmdi-caret-right-circle"></i></a>
                        </div>
                        <div class="tile">
                            <div class="tile-icon"><i class="zmdi zmdi-cutlery"></i></div>
                            <div class="tile-caption">
                                <span class="center-align">Registrar</span>
                                <p class="center-align">Platos</p>   
                            </div>
                            <a href="/RestaurantesWeb/RegistrarPlato.jsp" class="tile-link waves-effect waves-light">Ir a registrar&nbsp; <i class="zmdi zmdi-caret-right-circle"></i></a>
                        </div>
                        <div class="tile">
                            <div class="tile-icon"><i class="zmdi zmdi-assignment"></i></div>
                            <div class="tile-caption">
                                <span class="center-align">Mostrar</span>
                                <p class="center-align">Platos</p>   
                            </div>
                            <a href="/RestaurantesWeb/PlatoControlador" class="tile-link waves-effect waves-light">Ir a mostrar&nbsp; <i class="zmdi zmdi-caret-right-circle"></i></a>
                        </div>
                        <div class="tile">
                            <div class="tile-icon"><i class="zmdi zmdi-accounts-list-alt"></i></div>
                            <div class="tile-caption">
                                <span class="center-align">Mostrar</span>
                                <p class="center-align">Usuarios</p>   
                            </div>
                            <a href="/RestaurantesWeb/UsuarioControlador" class="tile-link waves-effect waves-light">Ir a mostrar &nbsp; <i class="zmdi zmdi-caret-right-circle"></i></a>
                        </div>

                    </div>   
                </article>



            </div>

            <div class="NavLateralDivider"></div>
            <div class="footer-copyright">
                <div class="container center-align">
                    © 2017 Restaurantes USA Web App
                </div>
            </div>



            <!-- Sweet Alert JS -->
            <script src="js/sweetalert.min.js"></script>

            <!-- jQuery -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
            <script>window.jQuery || document.write('<script src="js/jquery-2.2.0.min.js"><\/script>')</script>

            <!-- Materialize JS -->
            <script src="js/materialize.min.js"></script>

            <!-- Malihu jQuery custom content scroller JS -->
            <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>

            <!-- MaterialDark JS -->
            <script src="js/main.js"></script>
    </body>
</html>
