<%-- 
    Document   : RegistroUsuario
    Created on : 13/04/2017, 08:40:27 AM
    Author     : Jose
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Restaurantes App | Registro</title>
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/materialize.min.css">
        <link rel="stylesheet" href="css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
        <link rel="stylesheet" href="css/sweetalert.css">
        <link rel="stylesheet" href="css/style.css">
    </head>

    <body class="font-cover" id="registro">
        
        <%
                if (request.getAttribute("insercion") != null) {
                    String respuesta = (String) request.getAttribute("insercion");
                    if (respuesta.equals("OK")) {
                        out.print("Insercion exitosa");

                    } else if (respuesta.equals("NO")) {
                        out.print("Error al insertar");
                    }
                }

        %>

        <div class="container-login center-align">
            <div style="margin:15px 0;">
                <i class="zmdi zmdi-account-circle zmdi-hc-5x"></i>
                <p>Registro de usuario</p>   
            </div>
            
            <form action="UsuarioControlador" method="post">
                <div class="input-field col s6">
                    <input type="text" name="nombre">
                    <label for="first_name"><i class="zmdi zmdi-account"></i>&nbsp; Nombre</label>
                </div>
                <div class="input-field col s12">
                    <input type="text" name="correo">
                    <label for="email"><i class="zmdi zmdi-account-box-mail"></i>&nbsp; Correo</label>
                </div>
                <div class="input-field col s6">
                    <input type="text" name="nick">
                    <label for="last_name"><i class="zmdi zmdi-account-circle"></i>&nbsp; Nick</label>
                </div>
                <div class="input-field col s12">
                    <input type="password" name="password">
                    <label for="password"><i class="zmdi zmdi-lock"></i>&nbsp; Password</label>
                </div>
                <button class="waves-effect waves-teal btn-flat">Registrarme&nbsp; <i class="zmdi zmdi-mail-send"></i></button>
            </form>
            <div class="divider" style="margin: 20px 0;"></div>
            <a href="/RestaurantesWeb/UsuarioLogin.jsp">Iniciar Sesion</a>
        </div>

        <!-- Sweet Alert JS -->
        <script src="js/sweetalert.min.js"></script>

        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/jquery-2.2.0.min.js"><\/script>')</script>

        <!-- Materialize JS -->
        <script src="js/materialize.min.js"></script>

        <!-- Malihu jQuery custom content scroller JS -->
        <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>

        <!-- MaterialDark JS -->
        <script src="js/main.js"></script>

        <a href="/RestaurantesWeb/index.html" class="btn btn-round btn-default">Home</a>

</body>
</html>